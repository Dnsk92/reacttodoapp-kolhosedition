const todos = (state = [], action) => {
  console.log("TODOSSS")
  switch (action.type) {
    case 'FETCH_TODOS':
      return action.todos
    case 'ADD_TODO':
      return [...state,...action.todo]
    case 'DELETE_TODO':
      return state.filter((item) => item.id !== action.id)
    case 'COMPLETE_TASK':
      return state.map(todo => (todo.id === action.id) ? ({ ...todo, completed: !todo.completed }) : todo)
    case 'FIND_TASK':
      return action.title !== "" ? state.filter((item) => item.title.indexOf(action.title) !== -1) : state
    case 'UPDATE_TITLE':
      return action.title !== "" ? state.map(todo => (todo.id === action.id) ? ({ ...todo, title: action.title }) : todo) : state
    default:
      return state
  }
}

export default todos