import { combineReducers } from 'redux'
import todos from './todos'
import todo from './todo'
import filter from './filter'

const todoApp = combineReducers({ todo, todos, filter })

export default todoApp