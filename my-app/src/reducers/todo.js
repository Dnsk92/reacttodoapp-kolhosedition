const initialState = {
  editedId: null,
  // title: null
}

const todo = (state = initialState, action) => {
  switch (action.type) {
    case 'EDIT_TITLE':
      return { ...state, editedId: action.id }
    /*
  case 'UPDATE_TITLE':
    return {...state, title: action.title}
    */
    default:
      return state
  }
}



export default todo