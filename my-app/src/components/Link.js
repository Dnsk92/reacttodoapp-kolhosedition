import React from 'react';

const Link = ({ children, filter, onClickFilter }) => (
  <a href="#" onClick={() => onClickFilter(filter)}>
    {console.log("filter == " + filter)}
    {children}
  </a>
)

export default Link