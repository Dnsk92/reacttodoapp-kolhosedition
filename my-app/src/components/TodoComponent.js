import React from 'react';
import TodoListItem from './TodoListItem';
import Footer from './Footer';
import TodoListItemContainer from '../containers/TodoListItemContainer'

const TodoComponent = ({ AddTodoTask, FindTodoTask,addTodoThunk,fetchTodosThunk,findTodoThunk }) => (
  <div className="todo-component">
    <div>
      <div className="add-task-input">
        <input placeholder="Add Task" onKeyPress={(event) => event.key == "Enter" ? addTodoThunk(event.target.value) : ""} />
      </div>
      <div className="find-task-input">
      {/*  <input placeholder="Find Task" onKeyPress={(event) => FindTodoTask(event)} /> */}
        <input placeholder="Find Task" onKeyPress={(event) => event.key == "Enter" ? findTodoThunk(event.target.value) : ""}/>
      </div>
      <div>
        <input type="button" value="fetch todos" onClick={() => fetchTodosThunk()}/>
      </div>
    </div>
    <div>
      <label className="todo-label">ToDo Task List</label>
      <TodoListItemContainer />
      <Footer />
    </div>
  </div>
)

export default TodoComponent