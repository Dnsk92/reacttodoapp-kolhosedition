import React from 'react';
import LinkContainer from '../containers/LinkContainer'

const Footer = () => (
  <div className="footer">
    <LinkContainer filter="SHOW_ALL">
      All
    </LinkContainer>
    &nbsp;
    <LinkContainer filter="SHOW_COMPLETED">
      Complete
    </LinkContainer>
    &nbsp;
    <LinkContainer filter="SHOW_ACTIVE">
      Active
    </LinkContainer>
  </div>
)

export default Footer