import React from 'react';
const TodoItem = ({ id, completed, title, editedId, onClickCompleteTodo, onClickDeleteTodo, doubleClickEditTitle, onChangeUpdateTitle, onKeyPressEndEditTitle }) => (
  <div className="todo-item">
    <div className="todo-item-checkbox">
      <input type="checkbox" checked={completed} onClick={onClickCompleteTodo}/>
    </div>
    <div className="todo-item-text">
      {completed ? (<div className={(editedId !== null && id == editedId) ? "todo-text-label hidden" : "todo-text-label-complete"}><label>{title}</label></div>) :
        (<div className={(editedId !== null && id == editedId) ? "todo-text-label hidden" : "todo-text-label"} onDoubleClick={doubleClickEditTitle}>
          <label>{title}</label>
        </div>
        )}
      <div className={(editedId !== null && id == editedId) ? "text-input" : "text-input hidden"}>
        <input type="text" onChange={onChangeUpdateTitle} onKeyPress={onKeyPressEndEditTitle} />
      </div>
    </div>
    {id !== editedId ? <div className="destroy" onClick={onClickDeleteTodo}></div> : null}
  </div>
)
export default TodoItem