import React from 'react';
import TodoListItem from './TodoListItem';
import TodoComponent from './TodoComponent';
import TodoComponentContainer from '../containers/TodoComponentContainer';

const App = () => (
  <div className="App">
    <div className="App-header">

      <h2>TODO Application</h2>
    </div>
    <TodoComponentContainer />
  </div>
)

export default App