import React from 'react';
import TodoItem from './TodoItem';

const TodoListItem = ({ todos, itemState, onClickCompleteTodo, onClickDeleteTodo, doubleClickEditTitle, updateTitle, onKeyPressEndEditTitle }) => (
  <div className="ToDoListItemComponent">
    {console.log(todos)}
    {todos.map(todo => <TodoItem key={todo.id}
      {...todo}
      editedId={itemState.editedId}
      onClickCompleteTodo={() => onClickCompleteTodo(todo.id)}
      onClickDeleteTodo={() => onClickDeleteTodo(todo.id)}
      doubleClickEditTitle={() => doubleClickEditTitle(todo.id)}
      onChangeUpdateTitle={(event) => updateTitle(todo.id, event)}
      onKeyPressEndEditTitle={(event) => onKeyPressEndEditTitle(event)} />)}
  </div>
)


export default TodoListItem