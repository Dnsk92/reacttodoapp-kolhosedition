
export default class ToDoService {

  constructor(key) {
    this.key = key;
    this.toDos = this.getAll();
  }

  getToDos() {
     let todos = JSON.parse(localStorage.getItem(this.key));
    return this.toDos;
  }

  createUUID() {
    var s = [];
    var hexDigits = "0123456789ABCDEF";
    for (var i = 0; i < 32; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[12] = "4";
    s[16] = hexDigits.substr((s[16] & 0x3) | 0x8, 1);
 
    var uuid = s.join("");
    return uuid;
}

  completeTask(itemId) {
    this.toDos.map((item) => item.id == itemId ? (item.completed == true ? item.completed = false : item.completed = true) : item);
    localStorage.setItem(this.key,JSON.stringify(this.toDos));
  }

  add(title) {
    const toDo = {
      id: this.createUUID(),
      title: title,
      completed: false
    };
    this.toDos = this.toDos.concat(toDo);
    return localStorage.setItem(this.key,JSON.stringify(this.toDos));
  }

  getAll() {
    let store = localStorage.getItem(this.key);
    if(store == null || store == "") {
      return [];
    } else {
      return JSON.parse(store);
    }
  }

  save(itemId,titleNew) {
      this.toDos.map((item) => item.id == itemId ? item.title = titleNew : item);
      localStorage.setItem(this.key,JSON.stringify(this.toDos));
  }

  search(title) {
    let todos = JSON.parse(localStorage.getItem(this.key));
    if(title == "") {
      this.toDos = todos;
    } else {
      this.toDos = todos.filter((todo) => todo.title.indexOf(title) !== -1);
    }
  }

  delete(itemId) {
    let todos = JSON.parse(localStorage.getItem(this.key));
    this.toDos = todos.filter((item) => item.id !== itemId);
    localStorage.setItem(this.key,JSON.stringify(this.toDos));
  }
}