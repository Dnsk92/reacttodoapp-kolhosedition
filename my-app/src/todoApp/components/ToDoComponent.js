import React, { Component } from 'react';
import ToDoFooterComponent from './ToDoFooterComponent';
import ToDoListItemComponent from './ToDoListItemComponent';

export default class ToDoComponent extends Component {

constructor(props) {
    super(props);
    this.ToDoService = window.ToDoService;
    this.handlerAddToDoKeyPressEnter = this.handlerAddToDoKeyPressEnter.bind(this);
    this.handlerFindToDoKeyPressEnter = this.handlerFindToDoKeyPressEnter.bind(this);
    this.editedItem = this.editedItem.bind(this);
    this.updateTitle = this.updateTitle.bind(this);
    this.completeTask = this.completeTask.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
    this.state = {
      todos: this.ToDoService.getToDos(),
      editedId: null
    }
}

editedItem(id) {
  this.setState({
    editedId: id
  });
}

completeTask(itemId,isComplete){
  this.ToDoService.completeTask(itemId,isComplete);
  this.setState({
    todos: this.ToDoService.getToDos()
  });
}

updateTitle(itemId,titleNew) {
  this.ToDoService.save(itemId,titleNew);
  this.setState({
    todos: this.ToDoService.getToDos()
  });
}

deleteItem(itemId) {
  this.ToDoService.delete(itemId);
  this.setState({
    todos: this.ToDoService.getToDos()
  });
}

handlerAddToDoKeyPressEnter(event) {
    if(this.inputAdd.value.trim() !== "") {
        let key = event.key;
        if(key == "Enter") {
          this.ToDoService.add(this.inputAdd.value.trim());
          this.inputAdd.value = "";
          
          this.setState({
            todos: this.ToDoService.getToDos()
          });
        }
      }
}

handlerFindToDoKeyPressEnter(event) {
      let key = event.key;
      if(key == "Enter") {
        this.ToDoService.search(this.inputFind.value.trim());
        this.setState({
          todos: this.ToDoService.getToDos()
        });
      }
}


    render() {
        return (
        <div className="todo-component">
          <div>
            <div className="add-task-input">
            <input placeholder="Add Task" onKeyPress={this.handlerAddToDoKeyPressEnter} ref={(input)=> this.inputAdd = input}/>
            </div>
            <div className="find-task-input">
            <input placeholder="Find Task" onKeyPress={this.handlerFindToDoKeyPressEnter} ref={(input)=> this.inputFind = input}/>
            </div>
          </div>
            <div>
              <label className="lable">ToDo Task List</label>
             {/* ----------------------------------------- */} 
              <ToDoListItemComponent todos = {this.state.todos} 
                                     editedId = {this.state.editedId}
                                     editedItem = {this.editedItem}
                                     updateTitle = {this.updateTitle}
                                     completeTask = {this.completeTask}
                                     deleteItem = {this.deleteItem}/>
            {/* ----------------------------------------- */}
              <ToDoFooterComponent itemsLeft = {this.state.todos.filter((item)=>item.completed == false).length}/>
          </div>
      </div>
        );
    }
}