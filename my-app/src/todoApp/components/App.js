import React, { Component } from 'react';
import ToDoComponent from './ToDoComponent';

class App extends Component {
/*
  constructor(props) {
    super(props);
    this.ToDoService = window.ToDoService;
    this.handlerKeyPressEnter = this.handlerKeyPressEnter.bind(this);
  }
*/
/*
  handlerKeyPressEnter(event) {
      if(this.input.value.trim() !== "") {
        let key = event.key;
        if(key == "Enter") {
          this.ToDoService.add("",this.input.value.trim());
          this.input.value = "";
        }
      }
  }
*/

  render() {
    return (
      <div className="App">
        <div className="App-header">
          
          <img src={this.props.logo} className="App-logo" alt="logo" />
          
          <h2>TODO Application</h2>
        </div>
        {/* -------------------------------------------------- */}
        <ToDoComponent />
      {/* ------------------------------------------------------------------- */}
      </div>
    );
  }
}

export default App;
