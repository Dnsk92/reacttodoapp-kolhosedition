import React, { Component } from 'react';
import ToDoItemComponent from './ToDoItemComponent.js'

export default class ToDoListItemComponent extends Component {

  constructor(props) {
    super(props);

  }

    render() {
        return (
            <div className="ToDoListItemComponent">
              {this.props.todos.map((item)=> <ToDoItemComponent id={item.id}
                                                          title={item.title}
                                                          completed={item.completed}
                                                          editedId={this.props.editedId}
                                                          editedItem={this.props.editedItem}
                                                          updateTitle={this.props.updateTitle}
                                                          completeTask={this.props.completeTask}
                                                          deleteItem={this.props.deleteItem}/>
                  )}
                {/* Complete Item */}
                {/*
                <div className="todo-item">
                   <div className="todo-item-checkbox">
                    <input type="checkbox" checked/>
                   </div>
                   <div className="todo-item-text">
                     <div className="todo-text-label-complete">
                        <label>task 1</label>
                     </div>
                    <div className="text-input hidden">
                      <input type="text"/>
                    </div>
                  </div>
                   <div className="destroy" ></div>
                </div>
                */}
              {/* ********************** */}  
              {/* Active item  */}
              {/*
              <div className="todo-item">
                   <div className="todo-item-checkbox">
                    <input type="checkbox"/>
                   </div>
                   <div className="todo-item-text">
                     <div className="todo-text-label">
                        <label>task 2</label>
                     </div>
                    <div className="text-input hidden">
                      <input type="text"/>
                    </div>
                  </div>
                   <div className="destroy" ></div>
                </div>
                */}
              {/* ********************** */}
              {/* Edit item */}
              {/*
              <div className="todo-item">
                   <div className="todo-item-checkbox">
                    <input type="checkbox"/>
                   </div>
                   <div className="todo-item-text">
                     <div className="todo-text-label hidden">
                        <label>task 3</label>
                     </div>
                    <div>
                      <input type="text" value="task 3"/>
                    </div>
                  </div>
                </div>
                */}
              {/* ********************** */}
            </div>
        );
    }
}