import React, { Component } from 'react';

export default class ToDoItemComponent extends Component {

  constructor(props) {
    super(props);
    this.handlerDoubleClick = this.handlerDoubleClick.bind(this);
    this.handlerChangeTitle = this.handlerChangeTitle.bind(this);
    this.handlerEndEditKeyPressEnter = this.handlerEndEditKeyPressEnter.bind(this);
    this.handlerCompleteTask = this.handlerCompleteTask.bind(this);
    this.handlerDeleteItem = this.handlerDeleteItem.bind(this);
    this.ToDoService = window.ToDoService;
    this.state = {
      title: this.props.title,
      deleteAccess: true
    };
  }

  handlerDoubleClick() {
    this.props.editedItem(this.props.id);
    this.setState({
       deleteAccess: false
    });
  }
  
  handlerChangeTitle(event) {
    this.setState({
      title: event.target.value
    });
  }

  handlerEndEditKeyPressEnter(event) {
    if(this.inputEdit.value.trim() !== "") {
      let key = event.key;
      if(key == "Enter") {
           this.props.editedItem(null);
           this.props.updateTitle(this.props.id,event.target.value);
           this.setState({
              deleteAccess: true
           }); 
      }
    }
  }

  handlerCompleteTask(event) {
    this.props.completeTask(this.props.id);
  }

  handlerDeleteItem() {
    if(window.confirm("Are you sure want delete "+ this.props.title +" task")) {
      this.props.deleteItem(this.props.id);
    }
  }


    render() {
        return (
                <div className="todo-item">
                   <div className="todo-item-checkbox">
                    <input type="checkbox" checked={this.props.completed} onClick={this.handlerCompleteTask}/>
                   </div>
                   <div className="todo-item-text">
                     {this.props.completed ? (<div className="todo-text-label-complete"><label>{this.props.title}</label></div>) :
                     (
                       <div className={(this.props.editedId !== null && this.props.id == this.props.editedId) ? "todo-text-label hidden" : "todo-text-label"} onDoubleClick={this.handlerDoubleClick}>
                        <label>{this.props.title}</label>
                       </div>
                     )}
                    <div className={(this.props.editedId !== null && this.props.id == this.props.editedId) ? "text-input" : "text-input hidden"}>
                      <input type="text" value={this.state.title} onChange={this.handlerChangeTitle} onKeyPress={this.handlerEndEditKeyPressEnter} ref={(input)=> this.inputEdit = input}/>
                    </div>
                  </div>
                   {this.state.deleteAccess ? <div id="destroy_button" className="destroy" onClick={this.handlerDeleteItem}></div> : null}
                </div>
            
        );
    }
}