import React, { Component } from 'react';


export default class ToDoFooterComponent extends Component {

    constructor(props) {
        super(props);
        this.ToDoService = window.ToDoService;
    }


    render() {
        return (
              <div className="todo-footer">
                  {this.props.itemsLeft} item left
              </div>
        );
    }
}