import { connect } from 'react-redux'
import { completeTodo, deleteTodo, updateTitle } from '../actions/todos'
import { editTitle } from '../actions/todo'
import TodoListItem from '../components/TodoListItem'

const filterTodos = (todos, filter) => {
  switch (filter) {
    case "SHOW_ALL":
      return todos
    case "SHOW_COMPLETED":
      return todos.filter(t => t.completed)
    case 'SHOW_ACTIVE':
      return todos.filter(t => !t.completed)
  }
}


const mapStateToProps = (state) => ({
  todos: filterTodos(state.todos, state.filter),
  itemState: state.todo
})

const mapDispatchToProps = (dispatch) => ({
  onClickCompleteTodo: (id) => {
    dispatch(completeTodo(id))
  },
  onClickDeleteTodo: (id) => {
    dispatch(deleteTodo(id))
  },
  doubleClickEditTitle: (id) => {
    dispatch(editTitle(id))
  },
  onKeyPressEndEditTitle: (event) => {
    let key = event.key;
    if (key == "Enter") {
      dispatch(editTitle(null))
    }
  },
  updateTitle: (id, event) => {
    dispatch(updateTitle(id, event.target.value))
  }
})


const TodoListItemContainer = connect(mapStateToProps, mapDispatchToProps)(TodoListItem)

export default TodoListItemContainer
