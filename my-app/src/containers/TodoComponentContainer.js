import { connect } from 'react-redux'
import { addTodo, findTask } from '../actions/todos'
import TodoComponent from '../components/TodoComponent'
import {addTodoThunk} from '../thunk/thunks'
import * as actions from '../thunk/thunks'
/*
const mapDispatchToProps = dispatch => ({
  AddTodoTask: (event) => {
    let key = event.key;
    if (key == "Enter") {
      let title = event.target.value;
      event.target.value = "";
      dispatch(addTodo(title))
    }
  },
  FindTodoTask: (event) => {
    let key = event.key;
    if (key == "Enter") {
      let title = event.target.value;
      event.target.value = "";
      dispatch(findTask(title))
    }
  }
})
*/
const TodoComponentContainer = connect(null, actions)(TodoComponent)

export default TodoComponentContainer