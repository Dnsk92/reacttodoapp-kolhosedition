import { connect } from 'react-redux'
import Link from '../components/Link'
import { filterTodo } from '../actions/filter'




const mapDispatchToProps = dispatch => ({
  onClickFilter: (filter) => {
    dispatch(filterTodo(filter));
  }
});

const LinkContainer = connect(null, mapDispatchToProps)(Link)

export default LinkContainer