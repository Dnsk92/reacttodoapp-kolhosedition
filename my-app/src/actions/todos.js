export function addTodo(title) {
  return {
    type: "ADD_TODO",
    title
  }
}

export function deleteTodo(id) {
  return {
    type: "DELETE_TODO",
    id
  }
}

export function completeTodo(id) {
  return {
    type: "COMPLETE_TASK",
    id
  }
}

export function findTask(title) {
  return {
    type: "FIND_TASK",
    title
  }
}

export function updateTitle(id, title) {
  return {
    type: "UPDATE_TITLE",
    id,
    title
  }
}