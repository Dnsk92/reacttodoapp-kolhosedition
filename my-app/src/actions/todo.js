export function newTitle(id, title) {
  return {
    type: "NEW_TITLE",
    title,
    id
  }
}

export function editTitle(id) {
  return {
    type: "EDIT_TITLE",
    id
  }
}

export function updateTitle(title) {
  return {
    type: "UPDATE_TITLE",
    title
  }
}
