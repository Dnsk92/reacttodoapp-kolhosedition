const DATABASE_NAME = 'todoappdb_test';
const API_KEY = 'pTxU36pyA_l5lh_-CRWDnb9fkYmza_7a';
const API_URL = `https://api.mlab.com/api/1/databases/${DATABASE_NAME}/collections`;

const defaultOpts = {
    headers: {
        'Content-type': 'application/json'
    }
}

export function executeRequest(action,opts) {
    const acctualOpts = {...defaultOpts,...opts};
    return fetch(`${API_URL}/${action}?apiKey=${API_KEY}`,acctualOpts)
                .then(res => res.json());
}