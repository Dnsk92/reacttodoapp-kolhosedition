import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import './styles/todocomponent.css';
import './styles/App.css';
import './styles/todoListItemComponent.css';
import App from './components/App'
import todoService from './services/todoService'
import { store } from './store'

window.todoService = new todoService();


render(
  <Provider store={store}>
    <App />
  </Provider>,

  document.getElementById('root')
)