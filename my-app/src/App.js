import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {


  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>TODO Application</h2>
        </div>
        {/*
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        */}
        <div className="todo-component">
          <div>
            <div className="add-task-input">
            <input placeholder="Add Task"/>
            </div>
            <div className="find-task-input">
            <input placeholder="Find Task"/>
            </div>
          </div>
            <div>
              <label className="lable">ToDo Task List</label>
              <div className="ToDoListItemComponent">
                {/* Complete Item */}
                <div className="todo-item">
                   <div className="todo-item-checkbox">
                    <input type="checkbox" checked/>
                   </div>
                   <div className="todo-item-text">
                     <div className="todo-text-label-complete">
                        <label>task 1</label>
                     </div>
                    <div className="text-input hidden">
                      <input type="text"/>
                    </div>
                  </div>
                   <div className="destroy" ></div>
                </div>
              {/* ********************** */}  
              {/* Active item  */}
              <div className="todo-item">
                   <div className="todo-item-checkbox">
                    <input type="checkbox"/>
                   </div>
                   <div className="todo-item-text">
                     <div className="todo-text-label">
                        <label>task 2</label>
                     </div>
                    <div className="text-input hidden">
                      <input type="text"/>
                    </div>
                  </div>
                   <div className="destroy" ></div>
                </div>
              {/* ********************** */}
              {/* Edit item */}
              <div className="todo-item">
                   <div className="todo-item-checkbox">
                    <input type="checkbox"/>
                   </div>
                   <div className="todo-item-text">
                     <div className="todo-text-label hidden">
                        <label>task 3</label>
                     </div>
                    <div>
                      <input type="text" value="task 3"/>
                    </div>
                  </div>
                  <div className="destroy" ></div>
                </div>
              {/* ********************** */}
            </div>
          </div>
      </div>
      </div>
    );
  }
}

export default App;
