import reducers from './reducers'
import { createStore, applyMiddleware } from 'redux'
import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk';

const logger = createLogger({
  diff: true,
})

export const store = createStore(reducers, applyMiddleware(thunk))