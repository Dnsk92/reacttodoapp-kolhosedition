import {executeRequest} from '../api/api';
import {findTask} from '../actions/todos';
//import {addTodo} from '../actions/todos';

export function addTodoThunk(title) {

    let todo = {
        id: window.todoService.createUUID(),
        title: title,
        completed: false
    }

    let opts = {
        method: "POST",
        body: JSON.stringify(todo)
    }
    
    executeRequest("todos",opts);
    return dispatch => {
                    executeRequest("todos")
                        .then(data => dispatch(addTodo(data)));
                };
}

export function fetchTodosThunk() {
    return dispatch => {
        executeRequest("todos")
            .then(data => dispatch(fetchTodos(data)))
    }
}

export function findTodoThunk(title) {
    return dispatch => {
        dispatch(findTask(title))
    }
}

export function fetchTodos(todos) {
    return {
        type: "FETCH_TODOS",
        todos
    }
}

export function addTodo(todo) {
    return {
      type: "ADD_TODO",
      todo
    };
  }
